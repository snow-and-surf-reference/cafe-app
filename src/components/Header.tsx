import {
	IonBadge,
	IonButton,
	IonButtons,
	IonHeader,
	IonIcon, IonLabel,
	IonMenuToggle, IonTitle,
	IonToolbar
} from "@ionic/react";
import { cartOutline, menuOutline } from "ionicons/icons";
import { useHistory } from "react-router";
import { useAppSelector } from "../app/hooks";
import { selectAllCartItems } from "../app/slices/cartSlice";

const Header: React.FC = () => {
	const history = useHistory();
	const carts = useAppSelector(selectAllCartItems);

	return (
		<IonHeader color="dark" className="dark">
			<IonToolbar color="dark" className="fluid">
				<IonTitle
					onClick={() => {
						history.push(`/`);
					}}
					style={{
						cursor: "pointer",
					}}
				>
					<IonLabel>{`S&S Cafe`}</IonLabel>
				</IonTitle>
				<IonButtons slot="start">
					<IonMenuToggle menu="menu">
						<IonButton fill="clear" color="light">
							<IonIcon icon={menuOutline} slot="icon-only" />
						</IonButton>
					</IonMenuToggle>
				</IonButtons>
				<IonButtons slot="end">
					<IonButton
						onClick={() => {
							history.push(`/checkoutCart`)
						}}
					>
						<IonIcon slot="start" icon={cartOutline} />
						{carts.length ? <IonBadge slot="end" color={'warning'} >{carts.length}</IonBadge> : null}
					</IonButton>
				</IonButtons>
			</IonToolbar>
		</IonHeader>
	);
};

export default Header;
