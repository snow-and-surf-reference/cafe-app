import {
    IonBreadcrumb,
    IonBreadcrumbs,
    IonCard,
    IonCardContent,
    IonContent,
    IonItem,
    IonLabel,
    IonList,
    IonPage
} from "@ionic/react";
import * as CafeTypes from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { useEffect, useState } from "react";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { BsFillBagCheckFill } from 'react-icons/bs';
import { useHistory, useParams } from "react-router";
import { useAppSelector } from "../app/hooks";
import { selectAllCafeCats, selectAllCafeItems } from "../app/slices/cafeSlice";
import Header from "../components/Header";
import ImageComp from "../components/ImageComp";

import { addCartItem } from "../app/slices/cartSlice";
import { store } from "../app/store";
import './CatItems.scss';

const CatItems: React.FC = () => {
	const history = useHistory();
	const cats: CafeTypes.CafeCat[] = useAppSelector(selectAllCafeCats);
    const items: CafeTypes.CafeItem[] = useAppSelector(selectAllCafeItems);
	const { catId } = useParams<{ catId: string }>();
	const [cat, setCat] = useState<CafeTypes.CafeCat | null>(null);
    const [toasts, setToasts] = useState<{ count: number, toasts: string[] }>({
        count: 0,
        toasts: []
    });

	useEffect(() => {
		if (cats.length <= 0 || !catId) {
			return;
		}
		const tempCat = cats.find((x) => x.id === catId);
		tempCat && setCat(tempCat);
	}, [catId, cats]);
    
    useEffect(() => {
        if (toasts.count === toasts.toasts.length) { return };
        var interval = setInterval(() => {
            const tempToasts = [...toasts.toasts];
            tempToasts.pop();
            setToasts(t => Object.assign({ ...t }, {
                count: { ...t }.count - 1,
                toasts: tempToasts
            }))
        }, 1500);
        return () => {
            clearInterval(interval);
        }
    }, [toasts]);

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				{cat ? (
					<>
						<IonCard color={"dark"}>
							<IonCardContent>
								<IonBreadcrumbs color={"light"}>
                                    <IonBreadcrumb
                                        style={{cursor: 'pointer'}}
										onClick={() => {
											history.push(`/menu`, { direction: "back" });
										}}
									>
										主目錄
									</IonBreadcrumb>
									<IonBreadcrumb>
										{cat.name.zh ? cat.name.zh : cat.name.en}
									</IonBreadcrumb>
								</IonBreadcrumbs>
							</IonCardContent>
						</IonCard>
						<br />
						{items.length ? (
							<div className="cat-item-container">
                                {[...items]
                                    .filter(k => k.cat.id === cat.id)
									.sort((a, b) => a.index - b.index)
									.map((x) => (
                                        <div
                                            key={`cat-${x.cat.id}-item-${x.id}`}
                                            className={`cat-item${x.outOfStock ? ' disabled' : ''}`}
                                        >
                                            <ImageComp
                                                src={x.images}
                                                width='160px'
                                                height="160px"
                                                fit="cover"
                                                style={{
                                                    margin: 0
                                                }}
                                            />
                                            <h4 className="name">{x.name.zh ? x.name.zh : x.name.en} <span className="price">${x.price.toLocaleString(undefined, {
											minimumFractionDigits: 2,
										})}</span></h4>
                                            {x.name.zh ? <p className="en-name">{x.name.en}</p> : null}
                                            <button
                                                className={`add-to-cart-button${x.outOfStock ? ' disabled' : ''}`}
                                                disabled={x.outOfStock}
                                                onClick={() => {
                                                    store.dispatch(addCartItem(x));
                                                    const tempToasts = [...toasts.toasts];
                                                    tempToasts.push(`已加入 "${x.name.zh ? x.name.zh : x.name.en}"`)
                                                    setToasts(t => Object.assign({ ...t }, {
                                                        toasts: tempToasts
                                                    }))
                                                }}
                                            >
                                                <AiOutlineShoppingCart size={18} />
                                                <p>Add to Cart</p>
                                            </button>
                                            {
                                                x.outOfStock ?
                                                    <>
                                                        <div className="sold-out-overlay">
                                                            <p className="sold-out-text">SOLD OUT</p>
                                                        </div>
                                                    </>
                                                    : null
                                            }
                                        </div>
									))}
							</div>
						) : (
							<IonList>
								<IonItem>
									<IonLabel>
										<h3>沒有任何產品提供</h3>
									</IonLabel>
								</IonItem>
							</IonList>
						)}
					</>
				) : null}
            </IonContent>
            <div className="toast-container">
                {
                    toasts.toasts.map((x, idx) => (
                        <div className="toast" key={`${idx}-${x}`}>
                            <BsFillBagCheckFill size={18} className="icon" />
                            <p>{x}</p>
                        </div>
                    ))
                }
            </div>

		</IonPage>
	);
};

export default CatItems;
