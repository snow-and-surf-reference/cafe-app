import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import * as CafeTypes from "@tsanghoilun/snow-n-surf-interface/types/cafe";

const newOrderTemplate: CafeTypes.CafeOrder = {
    id: 'new',
    createdAt: new Date(),
    phone: '', 
    items: [],
    orderDate: new Date(),
    orderNumber: '000',
    paymentMethod: 'online_cc',
    total: 0,
    takeaway: false,
    status: 'pending'
}
interface CartState {
    tempOrder: CafeTypes.CafeOrder;
    lastUpdated: Date;
}

const initialState: CartState = {
    tempOrder: JSON.parse(localStorage.getItem('tempOrder') || JSON.stringify(newOrderTemplate)),
    lastUpdated: new Date()
};

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        setAllCartItems: (state: CartState, action: PayloadAction<CafeTypes.CafeItem[]>) => {
            state.tempOrder.items = action.payload;
            localStorage.setItem('tempOrder', JSON.stringify(state.tempOrder))
        },

        addCartItem: (state: CartState, action: PayloadAction<CafeTypes.CafeItem>) => { 
            state.tempOrder.items.push(action.payload);
            localStorage.setItem('tempOrder', JSON.stringify(state.tempOrder))
        },
        resetTempOrder: (state: CartState) => {
            state.tempOrder = newOrderTemplate;
            localStorage.setItem('tempOrder', JSON.stringify(state.tempOrder))
        }
    },
});

export const selectAllCartItems = (state: RootState) => state.cart.tempOrder.items;
export const selectTempOrder = (state: RootState) => state.cart.tempOrder;

export const {
    setAllCartItems,
    addCartItem,
    resetTempOrder
} = cartSlice.actions;

export default cartSlice.reducer;
