import { getApps, initializeApp } from "firebase/app";
import {
	addDoc,
	collection,
	getDoc,
	getDocs,
	getFirestore,
	onSnapshot,
	query,
    where,
    doc,
    Timestamp,
	limit
} from "firebase/firestore";
import * as CafeTypes from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import * as VenueTypes from '@tsanghoilun/snow-n-surf-interface/types/venue';
import { fromDB } from "./helpers";
import { store } from "./store";
import {
	setAllCafeCats,
	setAllCafeItems,
	setCafeCatsIsInit,
	setCafeItemsIsInit,
    setOrderById,
} from "./slices/cafeSlice";
import randomize from "randomatic";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import { setCapacities } from "./slices/venueSlice";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const firebaseConfig = {
	apiKey: process.env.REACT_APP_Firebase_apiKey,
	authDomain: process.env.REACT_APP_Firebase_authDomain,
	projectId: process.env.REACT_APP_Firebase_projectId,
	storageBucket: process.env.REACT_APP_Firebase_storageBucket,
	messagingSenderId: process.env.REACT_APP_Firebase_messagingSenderId,
	appId: process.env.REACT_APP_Firebase_appId,
	measurementId: process.env.REACT_APP_Firebase_measurementId,
};

let allUnSubs: Array<() => void> = [];

export const initFirebase = async () => {
	if (getApps().length <= 0) {
		// Initialize Firebase
		initializeApp(firebaseConfig);
		globalListeners();
	} else {
		console.log(`Firebase App already previously initialised`);
	}
};

const cafeListener = async () => {
	const db = getFirestore();
	const catsQ = query(collection(db, `cafeCats`));
	const itemsQ = query(collection(db, `cafeItems`));
	const unSub = onSnapshot(catsQ, async (sss) => {
		let cats: CafeTypes.CafeCat[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			cats.push({ id, ...data } as CafeTypes.CafeCat);
		});
		store.dispatch(setAllCafeCats(cats));
		store.dispatch(setCafeCatsIsInit(true));
	});
	const unSub2 = onSnapshot(itemsQ, async (sss) => {
		let items: CafeTypes.CafeItem[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			items.push({ id, ...data } as CafeTypes.CafeItem);
		});
		store.dispatch(setAllCafeItems(items));
		store.dispatch(setCafeItemsIsInit(true));
	});
	allUnSubs.push(unSub);
	allUnSubs.push(unSub2);
};

export const payOrder = async (order: CafeTypes.CafeOrder, phone: string) => {
	// gen Code
	const genCode = async () => {
		const code = randomize(`0`, 3);
		const q = query(
			collection(getFirestore(), `cafeOrders`),
			where(`orderNumber`, `==`, String(code))
		);
		const codeDocs = await getDocs(q);
		if (codeDocs.size === 0) {
			return code;
		}
		let hasDupToday: CafeTypes.CafeOrder[] = [];
		codeDocs.forEach(async (ss) => {
			const data = fromDB(ss.data());
			const temp: CafeTypes.CafeOrder = {
				id: ss.id,
				...data,
			} as CafeTypes.CafeOrder;
			const sessionDate = dayjs.tz(temp.orderDate).valueOf();
			const todayStart = dayjs.tz(Date.now()).startOf("day").valueOf();
			const todayEnd = dayjs.tz(Date.now()).endOf("day").valueOf();
			if (sessionDate >= todayStart && sessionDate <= todayEnd) {
				hasDupToday.push(temp);
			}
		});
		if (hasDupToday.length === 0) {
			return code;
		}
		await genCode();
	};
	const newCode = await genCode();

	try {
		let tempOrder: CafeTypes.CafeOrder = Object.assign(
			{ ...order },
			{
				orderNumber: String(newCode),
                createdAt: dayjs().toDate(),
                orderDate: dayjs.tz(dayjs()).startOf('day').toDate(),
				total: [...order.items].reduce((p, v) => p + v.price, 0),
                status: "preparing",
                phone
			}
		);

		const { id, ...data } = tempOrder;
		const res = await addDoc(collection(getFirestore(), `cafeOrders`), data);
		return {
			result: "success",
			orderId: res.id,
			orderNumber: newCode,
		};
    } catch (error) {
        console.log(error);
        return {
            result: 'failed',
        }
    }
};

export const getOrderById = async (id: string) => {
    let order: CafeTypes.CafeOrder | undefined = undefined;
    const res = await getDoc(doc(getFirestore(), `cafeOrders/${id}`));
    if (res.exists()) {
        const data = fromDB(res.data());
        order = { id, ...data } as CafeTypes.CafeOrder;
    }
    return order;
}

export const listenToOrderById = async (id: string) => { 
    const existingOrder = store.getState().cafe.orders.find(x => x.id === id);
    if (existingOrder) {
        console.log('order already being listened, no subscription will be created');
        return
    };
    const unSub = onSnapshot(doc(getFirestore(), `cafeOrders/${id}`), async (ss) => {
        if (ss.exists()) {
            const data = fromDB(ss.data());
            const order = { id, ...data } as CafeTypes.CafeOrder;
            store.dispatch(setOrderById(order));
        }
    })
    allUnSubs.push(unSub);
}

export const searchOrdersByPhone = async (phone: string) => {
    const q = query(
        collection(getFirestore(), `cafeOrders`),
        where('orderDate', '>=', Timestamp.fromDate(dayjs.tz(dayjs()).startOf('day').toDate())),
        where('orderDate', '<=', Timestamp.fromDate(dayjs.tz(dayjs()).endOf('day').toDate())),
    );
    const sss = await getDocs(q);
    let orders: CafeTypes.CafeOrder[] = [];
    sss.forEach(ss => {
        const id = ss.id;
        const data = fromDB(ss.data());
        const order = { id, ...data } as CafeTypes.CafeOrder;
        if (order.phone === phone) {
            orders.push(order);
        }
    })
    return orders;
}

const capListener = async () => {
    const q = query(collection(getFirestore(), `capacities`), limit(1));
    const sss = await getDocs(q);
    if (sss.size !== 1) {
        return;
    }
    const docRef = doc(getFirestore(), `capacities/${sss.docs[0].id}`);
    const unSub = onSnapshot(docRef, (ss) => {
        if (!ss.exists()) {
            return;
        }
        store.dispatch(
            setCapacities({
                ...ss.data(),
            } as VenueTypes.Capacities)
        );
    });
    allUnSubs.push(unSub);
};

const globalListeners = () => {
	cafeListener();
	capListener();
};
