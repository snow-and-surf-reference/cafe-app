import {
	IonApp,
	IonContent,
	IonImg,
	IonPage,
	IonRouterOutlet,
	IonSplitPane,
	setupIonicReact
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Route, Switch } from "react-router-dom";
import Logo from "./assets/loadingIcon.svg";
import Menu from "./components/Menu";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/display.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";

/* Theme variables */
import { useEffect } from "react";
import { initFirebase } from "./app/firebase";
import "./theme/variables.css";

import { useAppSelector } from "./app/hooks";
import { selectCafeIsInit } from "./app/slices/cafeSlice";
import CafeMenu from "./pages/CafeMenu";
import CatItems from "./pages/CatItems";
import CheckoutCart from "./pages/CheckoutCart";
import OrderDetails from "./pages/OrderDetails";
import PaymentPage from "./pages/PaymentPage";

import Orders from "./pages/Orders";
import "./theme/style.scss";

setupIonicReact({
	mode: "ios",
});

const App: React.FC = () => {
	const loaded: boolean = useAppSelector(selectCafeIsInit);

	useEffect(() => {
		initFirebase();
	}, []);

	return (
		<>
			{!loaded ? (
				<IonApp>
					<IonPage>
						<IonContent color={"dark"}>
							<div
								style={{
									display: "flex",
									height: "100vh",
								}}
								className="ion-justify-content-center ion-align-items-center ion-text-center"
							>
								<IonImg
									src={Logo}
									className="flashing-logo"
									style={{
										width: "156px",
										height: "auto",
										maxWidth: "70vh",
									}}
								/>
							</div>
						</IonContent>
					</IonPage>
				</IonApp>
			) : (
				<IonApp>
					<IonReactRouter>
						<IonSplitPane contentId="main" when={false}>
							<Menu />
							<IonRouterOutlet id="main">
								<Switch>
									<Route exact path={`/`}>
										<CafeMenu />
									</Route>
									<Route exact path={`/menu`}>
										<CafeMenu />
									</Route>
									<Route path={`/menu/:catId`}>
										<CatItems />
									</Route>
									<Route exact path={`/checkoutCart`}>
										<CheckoutCart />
									</Route>
									<Route exact path={`/payment`}>
										<PaymentPage />
									</Route>
									<Route path={`/orders`} exact>
										<Orders />
									</Route>
									<Route path={`/orders/:id`}>
										<OrderDetails />
									</Route>
								</Switch>
							</IonRouterOutlet>
						</IonSplitPane>
					</IonReactRouter>
				</IonApp>
			)}
		</>
	);
};

export default App;
