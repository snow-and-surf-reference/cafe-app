import {
    IonContent,
    IonItem,
    IonLabel,
    IonList,
    IonPage
} from "@ionic/react";
import * as CafeTypes from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { useHistory } from "react-router";
import { useAppSelector } from "../app/hooks";
import { selectAllCafeCats, selectAllCafeItems } from "../app/slices/cafeSlice";
import Header from "../components/Header";
import ImageComp from "../components/ImageComp";

const CafeMenu: React.FC = () => {
    const history = useHistory()
    const cats: CafeTypes.CafeCat[] = useAppSelector(selectAllCafeCats);
    const items: CafeTypes.CafeItem[] = useAppSelector(selectAllCafeItems);

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonList>
					{[...cats]
						.sort((a, b) => a.index - b.index)
						.map((x) => (
                            <IonItem key={`menu-cat-${x.id}`} button lines="full"
                                onClick={() => {
                                    history.push(`/menu/${x.id}`)
                                }}
                            >
                                <ImageComp
                                    src={items.filter(y => y.cat.id === x.id).length ? [...items].sort((a,b) => a.index - b.index).filter(y => y.cat.id === x.id)[0].images : null}
                                    fit='cover'
                                />
								<IonLabel>
                                    <h2>{x.name.zh ? x.name.zh : x.name.en}</h2>
                                    <p>{x.name.en}</p>
								</IonLabel>
							</IonItem>
						))}
				</IonList>
			</IonContent>
		</IonPage>
	);
};

export default CafeMenu;
