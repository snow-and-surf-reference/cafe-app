import { IonCard, IonCardContent, IonLabel, IonSpinner } from "@ionic/react";

const LoadingCard: React.FC = () => {
  return (
    <IonCard>
      <IonCardContent className="ion-text-center">
        <IonLabel>Loading...</IonLabel>
        <br />
        <IonSpinner name="dots" />
      </IonCardContent>
    </IonCard>
  );
};

export default LoadingCard;
