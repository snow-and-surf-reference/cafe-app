import {
	IonBreadcrumb,
	IonBreadcrumbs,
	IonButton,
	IonButtons,
	IonCard,
	IonCardContent,
	IonContent,
	IonFooter,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonListHeader,
	IonPage,
	IonToggle,
	IonToolbar,
} from "@ionic/react";
import { cartOutline, trashOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useAppSelector } from "../app/hooks";
import { selectAllCartItems, setAllCartItems } from "../app/slices/cartSlice";
import { selectCapacities } from "../app/slices/venueSlice";
import { store } from "../app/store";
import Header from "../components/Header";

const CheckoutCart: React.FC = () => {
	const history = useHistory();
	const cartItemsRaw = useAppSelector(selectAllCartItems);
	const [items, setItems] = useState(cartItemsRaw);
	const [edit, setEdit] = useState(false);
	const caps = useAppSelector(selectCapacities);

	useEffect(() => {
		setItems(cartItemsRaw);
	}, [cartItemsRaw]);

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonCard color={"dark"}>
					<IonCardContent>
						<IonBreadcrumbs color={"light"}>
							<IonBreadcrumb
								style={{ cursor: "pointer" }}
								onClick={() => {
									history.push(`/menu`, { direction: "back" });
								}}
							>
								主目錄
							</IonBreadcrumb>
							<IonBreadcrumb>購物籃</IonBreadcrumb>
						</IonBreadcrumbs>
					</IonCardContent>
				</IonCard>
				<IonList lines="full">
					<IonListHeader>
						<IonLabel>我的購物籃</IonLabel>
					</IonListHeader>
					{items.length ? (
						<>
							<IonItem>
								<IonLabel>修改</IonLabel>
								<IonToggle
									slot="end"
									color={`secondary`}
									checked={edit}
									onIonChange={(e) => {
										setEdit(e.detail.checked);
									}}
								/>
							</IonItem>
							{items.map((x, idx) => (
								<IonItem key={`cart-item-${idx}-id-${x.id}`} lines="full">
									<IonLabel>
										<h3>1 X {x.name.zh ? x.name.zh : x.name.en}</h3>
										<p>{x.name.en}</p>
									</IonLabel>
									<IonButtons slot="end">
										<IonLabel>
											<h2>
												$
												{x.price.toLocaleString(undefined, {
													minimumFractionDigits: 2,
												})}
											</h2>
										</IonLabel>
										{edit ? (
											<IonButton
												color={"danger"}
												onClick={() => {
													const tempItems = [...items];
													tempItems.splice(idx, 1);
													store.dispatch(setAllCartItems(tempItems));
												}}
											>
												<IonIcon
													icon={trashOutline}
													size="small"
													slot="icon-only"
												/>
											</IonButton>
										) : null}
									</IonButtons>
								</IonItem>
							))}
						</>
					) : (
						<>
							<IonItem>
								<IonLabel>你的購物籃沒有任何產品。</IonLabel>
							</IonItem>
						</>
					)}
				</IonList>
			</IonContent>
			<IonFooter color="dark" className="dark">
				<IonToolbar className="fluid" color="dark">
					<IonItem color={`dark`} lines="none">
						<IonLabel slot="start">總數:</IonLabel>
						<IonLabel slot="end">
							<h2>
								<b>
									$
									{Object.values([...items])
										.reduce((p, v) => p + v.price, 0)
										.toLocaleString(undefined, {
											minimumFractionDigits: 2,
										})}
								</b>
							</h2>
						</IonLabel>
					</IonItem>
				</IonToolbar>
				<IonToolbar color={"dark"} className="fluid">
					{caps?.hidePos ? (
						<IonButton
							expand="block"
							size="large"
							color={"warning"}
							disabled={true}
							onClick={() => {}}
						>
							<IonIcon icon={cartOutline} slot="start" />
							<IonLabel>
								<h2>請往櫃檯落單</h2>
							</IonLabel>
						</IonButton>
					) : (
						<IonButton
							expand="block"
							size="large"
							color={"warning"}
							disabled={items.length <= 0}
							onClick={() => {
								history.push(`/payment`);
							}}
						>
							<IonIcon icon={cartOutline} slot="start" />
							<IonLabel>
								<h2>前往支付</h2>
							</IonLabel>
						</IonButton>
					)}
				</IonToolbar>
			</IonFooter>
		</IonPage>
	);
};

export default CheckoutCart;
