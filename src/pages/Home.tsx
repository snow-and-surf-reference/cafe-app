import { IonCard, IonContent, IonLabel, IonPage } from "@ionic/react"
import Header from "../components/Header"


const Home: React.FC = () => {
    return (
        <IonPage>
            <Header />
            <IonContent className="fluid">
                <IonCard>
                    <IonLabel>Hello!</IonLabel>
                </IonCard>
            </IonContent>
        </IonPage>
    )
}

export default Home;