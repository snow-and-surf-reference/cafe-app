import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import { RootState } from "../store";

export interface VenueState {
	isInit: boolean;
	capacities: VenueTypes.Capacities | undefined;
}

const initialState: VenueState = {
	isInit: false,
	capacities: undefined,
};

export const venueSlice = createSlice({
	name: "venue",
	initialState,
	reducers: {
		setCapacities: (
			state: VenueState,
			action: PayloadAction<VenueTypes.Capacities>
		) => {
			state.capacities = action.payload;
		},
	},
});

export const selectCapacities = (state: RootState) => state.venue.capacities;

// Action creators are generated for each case reducer function
export const { setCapacities } = venueSlice.actions;

export default venueSlice.reducer;
