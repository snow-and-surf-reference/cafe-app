import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import * as CafeTypes from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { listenToOrderById } from "../firebase";

interface CafeState {
    isCatsInnit: boolean;
    isItemsInnit: boolean;
    cats: CafeTypes.CafeCat[];
    items: CafeTypes.CafeItem[];
    orders: CafeTypes.CafeOrder[];
}

const initialState: CafeState = {
    isCatsInnit: false,
    isItemsInnit: false,
    cats: [],
    items: [],
    orders: []
};

export const cafeSlice = createSlice({
    name: "cafe",
    initialState,
    reducers: {
        setCafeCatsIsInit: (state: CafeState, action: PayloadAction<boolean>) => {
            state.isCatsInnit = action.payload;
        },
        setCafeItemsIsInit: (state: CafeState, action: PayloadAction<boolean>) => {
            state.isItemsInnit = action.payload;
        },
        setAllCafeCats: (state: CafeState, action: PayloadAction<CafeTypes.CafeCat[]>) => {
            state.cats = action.payload;
        },
        setAllCafeItems: (state: CafeState, action: PayloadAction<CafeTypes.CafeItem[]>) => {
            state.items = action.payload;
        },
        setAllCafeOrders: (state: CafeState, action: PayloadAction<CafeTypes.CafeOrder[]>) => {
            state.orders = action.payload;
        },
        setOrderById: (state: CafeState, action: PayloadAction<CafeTypes.CafeOrder>) => {
            const idx = state.orders.findIndex(x => x.id === action.payload.id);
            if (idx < 0) {
                state.orders.push(action.payload);
            } else {
                state.orders[idx] = action.payload;
            }
        }
    },
});

export const selectCafeIsInit = (state: RootState) => state.cafe.isCatsInnit && state.cafe.isItemsInnit;
export const selectAllCafeCats = (state: RootState) => state.cafe.cats;
export const selectAllCafeItems = (state: RootState) => state.cafe.items;
export const selectOrderById = (id: string) => (state: RootState) => {
    const order = state.cafe.orders.find(x => x.id === id);
    if (!order) {
        listenToOrderById(id);
        return null;
    }
    return order;
}


export const {
    setCafeCatsIsInit,
    setCafeItemsIsInit,
    setAllCafeCats,
    setAllCafeItems,
    setOrderById
} = cafeSlice.actions;

export default cafeSlice.reducer;
