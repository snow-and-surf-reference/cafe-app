import {
	IonBreadcrumb,
	IonBreadcrumbs,
	IonCard,
	IonCardContent,
	IonCol,
	IonContent,
	IonGrid,
	IonItem,
	IonLabel,
	IonList,
	IonPage,
	IonRow,
} from "@ionic/react";
import dayjs from "dayjs";
import { useHistory, useParams } from "react-router";
import { useAppSelector } from "../app/hooks";
import { selectOrderById } from "../app/slices/cafeSlice";
import Header from "../components/Header";
import LoadingCard from "../components/LoadingCard";

const OrderDetails: React.FC = () => {
	const history = useHistory();
	const { id } = useParams<{ id: string }>();
	const order = useAppSelector(selectOrderById(id));

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonCard color={"dark"}>
					<IonCardContent>
						<IonBreadcrumbs color={"light"}>
							<IonBreadcrumb
								style={{ cursor: "pointer" }}
								onClick={() => {
									history.push(`/orders`);
								}}
							>
								返回
							</IonBreadcrumb>
							<IonBreadcrumb>訂單</IonBreadcrumb>
						</IonBreadcrumbs>
					</IonCardContent>
				</IonCard>
				<IonGrid>
					<IonRow>
						{/* {order === null ? ( */}
						{order === null ? (
							<IonCol>
								<LoadingCard />
							</IonCol>
						) : order === undefined ? (
							<></>
						) : (
							<>
								<IonCol>
									<IonCard color={"light"}>
										<IonCardContent className="ion-text-center">
											<IonLabel>
												<p>訂單號碼</p>
											</IonLabel>
											<p className="large-order-no">{order.orderNumber}</p>
											<br />
											<IonItem
												lines="none"
												color={
													order.status === "preparing"
														? "secondary"
														: order.status === "ready"
														? "danger"
														: order.status === "collected"
														? "medium"
														: "medium"
												}
											>
												{/* <IonLabel>訂單狀態:</IonLabel> */}
												<IonLabel className="ion-text-center">
													{order.status === "preparing"
														? "準備中"
														: order.status === "ready"
														? "請到取餐處領取"
														: order.status === "collected"
														? "已完成"
														: "過期/已取消"}
												</IonLabel>
											</IonItem>
										</IonCardContent>
									</IonCard>
								</IonCol>
								<IonCol>
									<IonCard color={"light"}>
										<IonList color={"light"}>
											<IonItem lines="none" color={"light"}>
												<IonLabel>
													<p>總數</p>
												</IonLabel>
												<IonLabel slot="end">
													<h3>
														<b>
															$
															{order.total.toLocaleString(undefined, {
																minimumFractionDigits: 2,
															})}
														</b>
													</h3>
												</IonLabel>
											</IonItem>
											<IonItem lines="none" color={"light"}>
												<IonLabel>
													<p>點餐時間</p>
												</IonLabel>
												<IonLabel slot="end">
													<h3>
														<b>
															{dayjs(order.createdAt).format(
																"HH:mm | DD MMM YYYY"
															)}
														</b>
													</h3>
												</IonLabel>
											</IonItem>
										</IonList>
									</IonCard>

									<IonCard color={"light"}>
                                                <IonList color={"light"}>
                                                    {
                                                        order.items.map((x, idx) => (
                                                            <IonItem key={`order-${order.id}-item-${idx}-id-${x.id}`}
                                                                color={"light"}
                                                                lines='none'
                                                            >
                                                                <IonLabel>
                                                                    <h3>1 X {x.name.zh ? x.name.zh : x.name.en}</h3>
                                                                </IonLabel>
                                                            </IonItem>
                                                        ))
                                                    }
                                        </IonList>
									</IonCard>
								</IonCol>
							</>
						)}
					</IonRow>
				</IonGrid>
			</IonContent>
		</IonPage>
	);
};

export default OrderDetails;
