import {
	IonAlert,
	IonBreadcrumb,
	IonBreadcrumbs,
	IonButton,
	IonCard,
	IonCardContent, IonCheckbox,
	IonContent,
	IonFooter,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonList,
	IonLoading,
	IonPage,
	IonToolbar
} from "@ionic/react";
import { walletOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useAppSelector } from "../app/hooks";
import { resetTempOrder, selectTempOrder } from "../app/slices/cartSlice";
import { store } from "../app/store";
import Header from "../components/Header";
import PaymentFlexForm from "../components/PaymentFlexForm";
import TopupFailedModal from "../components/TopupFailedModal";
import TopupTermsModal from "../components/TopupTermsModal";

export interface PaymentFields {
	name: string;
	exp: string;
	address1: string;
	address2: string;
	country: string;
}

const PaymentPage: React.FC = () => {
	const history = useHistory();
	const [paying, setPaying] = useState("idle");
	const [phone, setPhone] = useState("");
	const [disablePay, setDisablePay] = useState(true);
	const tempOrder = useAppSelector(selectTempOrder);
	const [paymentFields, setPaymentFields] = useState<PaymentFields>({
		name: "",
		exp: "",
		address1: "",
		address2: "",
		country: "HK",
	});
	const [termsAgreed, setTermsAgreed] = useState(false);
	const [showTermsModal, setShowTermsModal] = useState(false);
	const [showPaymentFailedModal, setShowPaymentFailedModal] = useState(false);

	useEffect(() => {
		const nameValid = paymentFields.name !== "";
		const expValid = /([0]{1}[1-9]{1})|([1]{1}[0-2]{1})\/[0-9]{2}/g.test(
			paymentFields.exp
		);
		const addressValid = paymentFields.address1 !== "";
		const phoneValid = phone.length === 8;
		if (
			nameValid &&
			expValid &&
			addressValid &&
			termsAgreed && 
			phoneValid
		) {
			setDisablePay(false);
		} else {
			setDisablePay(true);
		}
	}, [phone, paymentFields, termsAgreed]);

	const handlePay = async () => {
		setPaying("paying");
		let payload = { ...paymentFields, order: tempOrder, phone };
		const res = await (window as any).attemptPay(payload);
		if (res && res.result === "AUTHORIZED") {
			console.log("Payment Success", res);
			store.dispatch(resetTempOrder());
			setPaying("idle");
			// go to success page
			history.push(`/orders/${res.orderId}`);
		} else {
			console.log("Payment Not Authorised", res);
			setPaying("failed");
			// show error alert
			// setShowPaymentFailedModal(true);
			return;
		}
	};

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonCard color={"dark"}>
					<IonCardContent>
						<IonBreadcrumbs color={"light"}>
							<IonBreadcrumb
								style={{ cursor: "pointer" }}
								onClick={() => {
									history.push(`/menu`, { direction: "back" });
								}}
							>
								主目錄
							</IonBreadcrumb>
							<IonBreadcrumb
								style={{ cursor: "pointer" }}
								onClick={() => {
									history.push(`/checkoutCart`, { direction: "back" });
								}}
							>
								購物籃
							</IonBreadcrumb>
							<IonBreadcrumb>付款</IonBreadcrumb>
						</IonBreadcrumbs>
					</IonCardContent>
				</IonCard>

				<IonCard>
					<IonItem lines="none">
						<IonLabel>
							<p>應付總數</p>
						</IonLabel>
						<IonLabel slot="end">
							<h2>
								$
								{Object.values([...tempOrder.items])
									.reduce((p, v) => p + v.price, 0)
									.toLocaleString(undefined, {
										minimumFractionDigits: 2,
									})}
							</h2>
						</IonLabel>
					</IonItem>
				</IonCard>

				<IonCard color={"light"}>
					<IonItem color={"light"} lines="none">
						<IonLabel color={phone ? "dark" : "danger"}>
							<p>請提供你的電話號碼</p>
						</IonLabel>
					</IonItem>
					<IonList inset>
						<IonItem>
							<IonInput
								type="tel"
								placeholder="e.g. 98765432"
								value={phone}
								onIonChange={(e) => {
									setPhone(e.detail.value!);
								}}
							/>
						</IonItem>
					</IonList>
				</IonCard>

				{/* <IonCard color={"light"}>
					<IonCardHeader>
						<IonCardTitle>信用卡支付</IonCardTitle>
					</IonCardHeader>
					<IonCardContent>
						<IonLabel>Payment form Goes here</IonLabel>
					</IonCardContent>
				</IonCard> */}

				<PaymentFlexForm
					fields={paymentFields}
					setFields={(newFields) => {
						setPaymentFields(newFields);
					}}
				/>

				<IonCard
					color={termsAgreed ? "warning" : "light"}
					style={{
						marginBottom: "4rem",
					}}
				>
					<IonItem color={termsAgreed ? "warning" : "light"} lines="none">
						<IonCheckbox
							checked={termsAgreed}
							onIonChange={(e) => {
								setTermsAgreed(e.detail.checked);
							}}
							slot="start"
						/>
						<IonLabel>
							<h3>{`我已閱讀及同意條款及細則。`}</h3>
						</IonLabel>
						<IonButton
							onClick={(e) => {
								e.preventDefault();
								e.stopPropagation();
								setShowTermsModal(true);
							}}
						>{`顯示`}</IonButton>
					</IonItem>
				</IonCard>
			</IonContent>
			<IonFooter color="dark" className="dark">
				<IonToolbar color={"dark"} className="fluid">
					<IonButton
						color={disablePay ? "dark" : "warning"}
						expand="block"
						size="large"
						disabled={disablePay}
						onClick={() => {
							handlePay();
						}}
					>
						<IonIcon icon={walletOutline} slot="start" />
						<IonLabel>
							<h1>立即付款</h1>
						</IonLabel>
					</IonButton>
				</IonToolbar>
			</IonFooter>
			<IonLoading
				isOpen={paying === "paying"}
				spinner={"dots"}
				message="付款中"
			/>
			<IonAlert
				isOpen={paying === "failed"}
				header="付款失敗"
				message={`交易不成功，請重新再試。`}
				onDidDismiss={() => {
					setPaying("idle");
				}}
				buttons={[
					{
						text: "明白",
						role: "confirm",
					},
				]}
			/>
			<TopupTermsModal
				showTermsModal={showTermsModal}
				setShowTermsModal={setShowTermsModal}
			/>
			<TopupFailedModal
				showPaymentFailedModal={showPaymentFailedModal}
				setShowPaymentFailedModal={setShowPaymentFailedModal}
			/>
		</IonPage>
	);
};

export default PaymentPage;
