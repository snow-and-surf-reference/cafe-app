import {
	IonCard,
	IonContent,
	IonInput,
	IonItem,
	IonLabel,
	IonList,
	IonListHeader,
	IonLoading,
	IonPage,
} from "@ionic/react";
import { CafeOrder } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { searchOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { searchOrdersByPhone } from "../app/firebase";
import Header from "../components/Header";

const Orders: React.FC = () => {
    const history = useHistory();
	const [phone, setPhone] = useState("");
	const [disableSearch, setDisableSearch] = useState(true);
    const [results, setResults] = useState<CafeOrder[]>([]);
    const [searching, setSearching] = useState(false);

	useEffect(() => {
		if (phone.length !== 8) {
			setDisableSearch(true);
		} else {
			setDisableSearch(false);
		}
	}, [phone]);

    const doSearch = async () => {
        setSearching(true);
        const res = await searchOrdersByPhone(phone);
        setSearching(false);
		setResults(res);
	};

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonCard color={"dark"}>
					<IonItem lines="none" color={"dark"}>
						<IonLabel className="ion-text-center">請提供電話號碼</IonLabel>
					</IonItem>
					<IonList inset>
						<IonItem>
							<IonInput
								type="tel"
								placeholder="e.g. 98765432"
								value={phone}
								onIonChange={(e) => setPhone(e.detail.value!)}
							/>
						</IonItem>
						<IonItem
							color={"warning"}
							button
							detailIcon={searchOutline}
                            disabled={disableSearch}
                            onClick={() => doSearch()}
						>
							<IonLabel>搜索</IonLabel>
						</IonItem>
					</IonList>
                </IonCard>
				{results.length ? (
					<IonList>
						<IonListHeader>
							<IonLabel>搜索結果</IonLabel>
                        </IonListHeader>
                        {
                            results.map(x => (
                                <IonItem
                                    key={`order-search-${x.id}`}
                                    button
                                    onClick={() => {
                                        history.push(`/orders/${x.id}`);
                                    }}
                                >
                                    <IonLabel>
                                        <h3><b>訂單 #{x.orderNumber}</b></h3>
                                    </IonLabel>
                                </IonItem>
                            ))
                        }
					</IonList>
                ) : null}
                <IonLoading isOpen={searching} spinner={'dots'} message={`搜索中`}/>
			</IonContent>
		</IonPage>
	);
};

export default Orders;
