import { IonModal, IonHeader, IonToolbar, IonTitle, IonContent, IonGrid, IonRow, IonCol, IonFooter, IonButton } from "@ionic/react"

interface Props{
    showTermsModal: boolean;
    setShowTermsModal: (bool: boolean) => void;
}

const TopupTermsModal: React.FC<Props> = (props) => {
    const { showTermsModal, setShowTermsModal } = props;

    return (
        <IonModal
				isOpen={showTermsModal}
				onDidDismiss={() => setShowTermsModal(false)}
			>
				<IonHeader>
					<IonToolbar>
						<IonTitle>{`Payment Terms & Conditions`}</IonTitle>
					</IonToolbar>
				</IonHeader>
				<IonContent>
					<IonGrid>
						<IonRow>
							<IonCol>
								<div>
									<h3>Payment Terms</h3>
									<p style={{ fontSize: "14px" }}>
										Sed ut perspiciatis unde omnis iste natus error sit
										voluptatem accusantium doloremque laudantium, totam rem
										aperiam, eaque ipsa quae ab illo inventore veritatis et
										quasi architecto beatae vitae dicta sunt explicabo. Nemo
										enim ipsam voluptatem quia voluptas sit aspernatur aut odit
										aut fugit, sed quia consequuntur magni dolores eos qui
										ratione voluptatem sequi nesciunt. Neque porro quisquam est,
										qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
										velit, sed quia non numquam eius modi tempora incidunt ut
										labore et dolore magnam aliquam quaerat voluptatem. Ut enim
										ad minima veniam, quis nostrum exercitationem ullam corporis
										suscipit laboriosam, nisi ut aliquid ex ea commodi
										consequatur? Quis autem vel eum iure reprehenderit qui in ea
										voluptate velit esse quam nihil molestiae consequatur, vel
										illum qui dolorem eum fugiat quo voluptas nulla pariatur?
									</p>
								</div>
							</IonCol>
						</IonRow>
					</IonGrid>
				</IonContent>
				<IonFooter>
					<IonToolbar>
						<IonButton expand="block" onClick={() => setShowTermsModal(false)}>
							Close
						</IonButton>
					</IonToolbar>
				</IonFooter>
			</IonModal>
    )
}

export default TopupTermsModal;