import { configureStore } from "@reduxjs/toolkit";
import cafeSlice from "./slices/cafeSlice";
import cartSlice from "./slices/cartSlice";
import venueSlice from "./slices/venueSlice";

export const store = configureStore({
    reducer: {
        cafe: cafeSlice,
        cart: cartSlice,
        venue: venueSlice
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;