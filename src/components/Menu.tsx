import { IonContent, IonIcon, IonItem, IonLabel, IonList, IonMenu } from "@ionic/react";
import {
	clipboardOutline,
	fastFoodOutline, homeOutline
} from "ionicons/icons";
import { useRef } from "react";
import { useHistory } from "react-router";

interface MenuItemProps {
	children?: any;
	onClick: () => void;
}
const MenuItem: React.FC<MenuItemProps> = (props) => {
	const { children, onClick } = props;
	return (
		<IonItem
			className="main-menu__item"
			color="transparent"
			button
			onClick={() => onClick()}
			detail={false}
		>
			{children}
		</IonItem>
	);
};

const Menu: React.FC = () => {
	const history = useHistory();
	const menu = useRef<HTMLIonMenuElement | null>(null);

	return (
		<IonMenu
			side="start"
			contentId="main"
			menuId="menu"
			type="reveal"
			color="dark"
			ref={menu}
		>
			<IonContent color="dark">
				<IonList
					lines="none"
					color="dark"
					style={{
						marginTop: `12vw`,
					}}
				>
					<MenuItem
						onClick={() => {
							menu.current?.close();
							history.push(`/`);
						}}
					>
						<IonIcon slot="start" size="small" icon={homeOutline} />
						<IonLabel>Home</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/menu`);
					}}>
						<IonIcon slot="start" size="small" icon={fastFoodOutline} />
						<IonLabel>Cafe Menu</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/orders`);
					}}>
						<IonIcon slot="start" size="small" icon={clipboardOutline} />
						<IonLabel>My Orders</IonLabel>
					</MenuItem>
				</IonList>
			</IonContent>
		</IonMenu>
	);
};

export default Menu;
