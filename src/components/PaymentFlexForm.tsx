import {
	IonCard,
	IonCol,
	IonGrid,
	IonInput,
	IonItem,
	IonLabel,
	IonList, IonRow, IonSelect, IonSelectOption
} from "@ionic/react";
import axios from "axios";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { useEffect } from "react";
import { PaymentFields } from "../pages/PaymentPage";
var countries = require("i18n-iso-countries");
countries.registerLocale(require("i18n-iso-countries/langs/en.json"));
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props{
	fields: PaymentFields
	setFields: (fields: PaymentFields) => void;
}

// Temp api for getting flex fields
// let apiUrl = `http://localhost:3000`;
let apiUrl = process.env.REACT_APP_PAYMENT_API || '';
const endpoint = `/flexFields`;

// countries code and name
const alpha2Codes: string[] = countries.getNames("en", { select: "official" });

export const PaymentFlexForm: React.FC<Props> = (props) => {
	const { fields, setFields } = props;

	useEffect(() => {
		const getFlexFields = async () => {
			const res = await axios.get(`${apiUrl}${endpoint}`);
			(window as any).updateFlexForm(res.data);
		};
        getFlexFields();
	}, []);

	return (
		<IonGrid>
			<IonRow>
				<IonCol>
					<IonLabel>
						<b>信用卡支付</b>
					</IonLabel>
				</IonCol>
			</IonRow>
			<IonRow>
				<IonCol>
					{/* Form goes here */}
					<IonCard
						color={"light"}
					>
						<IonGrid>
							<IonRow>
								<IonCol>
									<IonItem color={"light"} lines="none">
										<IonLabel>
											<h2>
												<b>信用卡資料</b>
											</h2>
										</IonLabel>
									</IonItem>
									<IonList inset>
										<IonItem>
											<IonLabel position="stacked">持卡人名稱</IonLabel>
											<IonInput
												type="text"
												placeholder="e.g. Chan Tai Man"
												value={fields.name}
												onIonChange={e => {
													setFields(Object.assign({ ...fields }, {
														name: e.detail.value!
													}));
												}}
											/>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">信用卡號碼</IonLabel>
											<div
												id="number-container"
												className="form-control"
												style={{ height: `32px` }}
											></div>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">有效日期</IonLabel>
											<IonInput
												type="text"
												value={fields.exp}
												placeholder={`MM/YY`}
												inputmode={`numeric`}
												onIonChange={e => {
													const value = (e.detail.value as string).replaceAll('/', '');
													let slashed = value;
													if (value && value.length > 2) {
														slashed = `${value.substring(0,2)}/${value.substring(2)}`
													}
													setFields(Object.assign({ ...fields }, {
														exp: slashed.substring(0, 5)
													}))
												}}
											/>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">CSV</IonLabel>
											<div
												id="securityCode-container"
												className="form-control"
												style={{ height: `32px` }}
											></div>
										</IonItem>
									</IonList>
								</IonCol>
								<IonCol>
									<IonItem color={"light"} lines="none">
										<IonLabel>
											<h2>
												<b>支付地址</b>
											</h2>
										</IonLabel>
									</IonItem>
									<IonList inset>
										<IonItem>
											<IonLabel position="stacked">地址 1</IonLabel>
											<IonInput
												type="text"
												value={fields.address1}
												onIonChange={e => {
													setFields(Object.assign({ ...fields }, {
														address1: e.detail.value!
													}));
												}}
											/>
										</IonItem>
										<IonItem>
											<IonLabel position="stacked">地址 2</IonLabel>
											<IonInput
												type="text"
												value={fields.address2}
												onIonChange={e => {
													setFields(Object.assign({ ...fields }, {
														address2: e.detail.value!
													}));
												}}
											/>
                                        </IonItem>
                                        <IonItem>
											<IonLabel position="stacked">國家</IonLabel>
                                            <IonSelect
												value={fields.country}
												onIonChange={(e) => {
													setFields(Object.assign({ ...fields }, {
														country: e.detail.value!
													}))
												}}
                                            >
                                                {
                                                    Object.entries({...alpha2Codes}).map((value) => (
                                                        <IonSelectOption key={`payment-country-${value[0]}`} value={value[0]}>{value[1]}</IonSelectOption>
                                                    ))
                                                }
                                            </IonSelect>
										</IonItem>
									</IonList>
								</IonCol>
							</IonRow>
						</IonGrid>
					</IonCard>
				</IonCol>
			</IonRow>
		</IonGrid>
	);
};

export default PaymentFlexForm;
